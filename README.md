<img style="float: right" align="right" src="https://gitlab.com/pastel-fablab/orga/raw/master/logo/fablab-logo-transparent-300dpi.png" width="350">

# Moto planneur autopiloté solaire

Réaliser un motoplaneur de 3 mètres d’envergure réalisant des vols d’endurances
- autopiloté ou piloté en mode assisté,
- capable de recharger ses batteries pendant le vol avec les panneaux solaires
- fabriqué dans un FabLab grâce à l’impression 3D et la découpe laser à moindre coût

![image-1.png](./image-1.png)

## Fiche projet 
Voir Présentation

## Matériel

Voir Présentation

## Description détaillée 

Voir Présentation

## Liens

Voir Présentation

## License

Ce projet est sous licence Apache 2.0. Voire le fichier [LICENSE](LICENSE.md) pour plus de détails
